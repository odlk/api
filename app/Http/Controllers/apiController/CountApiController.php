<?php

namespace App\Http\Controllers\apiController;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Projet;

class CountApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function usersCommunes($id)
    {
        $users = User::findOrFail($id);
        $userCommune = $users->IdCommune;
         //NB: 3 correspond a agent dans la table typeUsers
        //$usersCommunes = User::where('IdCommune',$userCommune)->where('IdTypeUtilisateur',3)->with(['commune:id,nom'])->get();

        $UserNomTypeUtilisateurAll= DB::table('users')
        ->join('type_utilisateurs', 'users.IdTypeUtilisateur', '=', 'type_utilisateurs.id')
        ->where('users.IdCommune',$userCommune)->where('type_utilisateurs.profil','agent')
        ->count();

        return response()->json($UserNomTypeUtilisateurAll,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function projetsCommunes()
    {
        $nbreProjet = Projet::count('id');
        return response()->json($nbreProjet ,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function informationsCommunes(Request $request, $idCommune)
    {
        $informationCommunes = DB::table('informations')
        ->where('IdCommune', $idCommune)
        ->count();
        return response()->json($informationCommunes ,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
