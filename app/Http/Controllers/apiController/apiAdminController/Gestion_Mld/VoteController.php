<?php

namespace App\Http\Controllers\apiController\apiAdminController\Gestion_Mld;

use App\Models\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class VoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vote = Vote::All()->orderByDesc('created_at')->get();
        return response()->json($vote,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $user, $option)
    {
        $input = $request->all();

        $validator = Validator::make($input,[
            'vote' => 'required',
            'IdUser'=>'required',
            'IdOption'=>'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'message'=>$validator->errors(),
                'status' => false,
            ]);
        }

        $idee = New Vote();
        $idee->IdUser = $request->input('IdUser');
        $idee->IdTheme = $request->input('IdOption');
        $idee->save();
        $point = 1;
       $points =DB::table("options")->where('id',$option)->update([
        'votes' => DB::raw('votes + '.$point)
 ]);

        return response()->json([
            "status"=>"True",
            "message"=>"Insertion réussie!",
            "proposIdees"=>$idee,
            "point"=>$points
        ],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $vote = Vote ::findOrFail($id);



        if(is_null($vote))
        {
            return response()->json(["status"=>"false","message"=>"Idée non trouvée!"],400);
        }

         // $votsonde= DB::table('sondages')
        //     ->join('options', 'sondages.id', '=', 'options.Idsondage')
        //     ->select('options.libelle', 'options.point', 'sondages.description')
        //     ->get();

        $votsonde = DB::table('options')
        ->where('id', $vote)
        ->join('sondages', 'sondages.id', '=', 'options.Idsondage')
        ->select('options.libelle', 'options.point', 'sondages.description')
        ->get();
        return response()->json($vote,200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
