<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Participer extends Model
{
    use HasFactory, SoftDeletes;

/**
 * Table properties
 */

    protected $table ='participer';
    protected $primaryKey = 'id';

	protected $fillable = [
		'montant'=>'int',
		'IdUser'=>'int',
        'IdCollecte'=>'int',
	];


	public function collecte()
	{
		return $this->belongsTo(Collecte::class,'IdCollecte','id');
	}

    public function user()
    {
        return $this->belongsTo(User::class,'IdUser','id');
    }

}
