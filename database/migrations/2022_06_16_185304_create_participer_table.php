<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticiperTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participer', function (Blueprint $table) {
            $table->id();
            $table->integer('montant');
            $table->foreignId('IdUser')->constrained('users');
            $table->foreignId('IdCollecte')->constrained('collectes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participer');
    }
}
